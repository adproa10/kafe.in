# Kafe.in

A reduced-complexity coffee shop application using Spring Boot. This project is part of the Advanced Programming (CSCM602023) course at Fasilkom UI.

[![pipeline status](https://gitlab.com/adproa10/kafe.in/badges/master/pipeline.svg)](https://gitlab.com/adproa10/kafe.in/-/commits/master) [![coverage report](https://gitlab.com/adproa10/kafe.in/badges/master/coverage.svg)](https://gitlab.com/adproa10/kafe.in/-/commits/master) [![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

## Authors

Group A10 - Class A - Advanced Programming 2019/2020 Even Semester course

1. [Apandaoswalen](https://gitlab.com/apanda.saragih)
2. [Nabila Ayu Dewanti](https://gitlab.com/nabilaayu)
3. [Rafi Muhammad Daffa](https://gitlab.com/skycruiser8)
4. [Talitha Luthfiyah Dhany Maheswari](https://gitlab.com/Talithamaheswari)

## Dependencies

This project requires Java Development Kit (JDK) 13.0.2 or higher to run. A Gradle installation of version 6.0.1 or higher is recommended, however you can run this project through the included Gradle Wrapper (<code>gradlew</code>). Every other dependencies are handled internally by Gradle.

## License

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2020, Faculty of Computer Science Universitas Indonesia

Permission to copy, modify, and share the works in this project are governed under the [BSD 3-Clause](https://opensource.org/licenses/BSD-3-Clause) license.
