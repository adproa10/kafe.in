package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.service;


import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.*;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model.BeverageModel;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.repository.BeverageRepository;
import id.ac.ui.cs.advprog.a10.kafein.coffeedecoration.LargeCoffee;
import id.ac.ui.cs.advprog.a10.kafein.coffeedecoration.LargeTea;
import id.ac.ui.cs.advprog.a10.kafein.coffeedecoration.MilkCoffee;
import id.ac.ui.cs.advprog.a10.kafein.coffeedecoration.SweetTea;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BeverageServiceImpl implements BeverageService {
  @Autowired
  private BeverageRepository beverageRepository;

  @Override
  public List<BeverageModel> findAll() {
    return beverageRepository.findAll();
  }

  @Override
  public Optional<BeverageModel> findBeverage(Long id) {
    return beverageRepository.findById(id);
  }

  @Override
  public void erase(Long id) {
    beverageRepository.deleteById(id);
  }

  @Override
  public BeverageModel rewrite(BeverageModel beverage) {
    beverageRepository.save(beverage);
    return beverage;
  }

  @Override
  public BeverageModel register(BeverageModel beverage) {
    beverageRepository.save(beverage);
    return beverage;
  }

  @Override
  public Beverage createBeverage(String jenis, String penyajian, List<String> extras){
    if (extras == null) {
      extras = new ArrayList<String>();
    }
    Beverage beverage;
    BeverageFactory factory;
    if (penyajian.equals("hot")){
      factory = new HotBeverageFactory();
    }
    else {
      factory = new IcedBeverageFactory();
    }

    if (jenis.equals("coffee")){
      beverage = factory.createCoffee("Coffee", 13000);

      for (String decor : extras) {
        if(decor.equals("milk")) {
          beverage = new MilkCoffee((Coffee) beverage);
        }
        if(decor.equals("upsize")) {
          beverage = new LargeCoffee((Coffee) beverage);
        }
      }
    }
    else{
      beverage = factory.createTea("Tea", 11000);
      if(extras.size() != 0) {
        for (String decor : extras) {
          if (decor.equals("milk")) {
            beverage = new SweetTea((Tea) beverage);
          }
          if (decor.equals("upsize")) {
            beverage = new LargeTea((Tea) beverage);
          }
        }
      }
    }
    return beverage;
  }

  @Override
  public BeverageModel addBeverage(Beverage beverage) {
    String[] beverage_string = beverage.getClass().getSimpleName().split("(?=[A-Z])");
    BeverageModel beverage_to_save = new BeverageModel(
            beverage.getName(),
            beverage.getSpecification(),
            beverage.getPrice(),
            BeverageModel.Type.valueOf(beverage_string[1].toUpperCase()),
            BeverageModel.Served.valueOf(beverage_string[0].toUpperCase())
    );
    return beverageRepository.save(beverage_to_save);
  }
}
