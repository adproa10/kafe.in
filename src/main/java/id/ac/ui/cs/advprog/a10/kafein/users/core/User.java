package id.ac.ui.cs.advprog.a10.kafein.users.core;

import java.util.ArrayList;
import java.util.List;

public class User implements TransactionObserver {
  private String name;
  private final String email;
  private String password;
  private final String role;
  private final List<Integer> transactionList;

  /**
   * Constructs a User object using four arguments: Name, Email, Password, and Role.
   * Can be used to create either customers or stores.
   *
   * @param name String - User name
   * @param email String - User email (Primary Key - Unchangeable)
   * @param password String - User password
   * @param role - User role (Unchangeable)
   */
  public User(String name, String email, String password, String role) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.role = role;
    this.transactionList = new ArrayList<>();
  }

  /**
   * Constructs a User object using three arguments: Name, Email, and Password.
   * Defaults to creating a User object with customer role.
   *
   * @param name String - User name
   * @param email String - User email (Primary key - unchangeable)
   * @param password String - User password
   */
  public User(String name, String email, String password) {
    this(name,email,password,"CUSTOMER");
  }

  /**
   * Frontend method for validating authenticity by password.
   *
   * @param password String - Password under check
   * @return boolean - Validity status
   */
  public boolean validatePassword(String password) {
    return this.password.equals(password);
  }

  public String getEmail() {
    return this.email;
  }

  public String getName() {
    return this.name;
  }

  public String getPassword() {
    return this.password;
  }

  public String getRole() {
    return this.role;
  }

  public List<Integer> getTransactionList() {
    return this.transactionList;
  }

  /**
   * Changes the name for a particular User object.
   * Please note that null and empty string values are not accepted.
   *
   * @param name String - new user name
   */
  public void setName(String name) {
    if (name != null && !name.equals("")) {
      this.name = name;
    }
  }

  /**
   * Changes the password for a particular User object.
   * Please note that null and empty string values are not accepted.
   *
   * @param password String - new user password
   */
  public void setPassword(String password) {
    if (password != null && !password.equals("")) {
      this.password = password;
    }
  }

  @Override
  public void addTransaction(int transactionId) {
    this.transactionList.add(transactionId);
  }

  @Override
  public void notify(int transactionId, String message) {
  // TODO: Implement change state instruction
  }
}
