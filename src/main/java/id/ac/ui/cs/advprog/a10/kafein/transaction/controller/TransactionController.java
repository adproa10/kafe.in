package id.ac.ui.cs.advprog.a10.kafein.transaction.controller;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import id.ac.ui.cs.advprog.a10.kafein.transaction.service.TransactionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class TransactionController {
  @Autowired
  private TransactionService transactionService;

  @GetMapping("/transaction")
  public String transactionPage(Model model) {
    List<TransactionModel> transactions = transactionService.findAll();
    model.addAttribute("transactions", transactions);
    return "website/transaction";
  }

  @RequestMapping(value = "/update/{id}", produces = "application/json",
                  method = {RequestMethod.PUT, RequestMethod.GET})
  public String updateTransaction(@PathVariable int id) {
    TransactionModel transactionModel = transactionService.getTransaction(id).get();
    transactionService.updateTransaction(transactionModel);
    return "redirect:/transaction/";
  }

  @GetMapping("/delete/{id}")
  public String delete(@PathVariable int id) {
    transactionService.deleteTransaction(id);
    return "redirect:/transaction/";
  }
}
