package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.controller;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Beverage;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model.BeverageModel;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.service.BeverageService;
import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import id.ac.ui.cs.advprog.a10.kafein.transaction.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping(path="/beverage")
public class BeverageController {

  @Autowired
  private BeverageService beverageService;

  @Autowired
  private TransactionService transactionService;

  @RequestMapping(path ="/add", method = RequestMethod.GET)
  public String tes(
          Model model){
    return "website/add-beverage";
  }

  @RequestMapping(path ="/home", method = RequestMethod.GET)
  public String apa(
          Model model){
    return "website/home";
  }

  @RequestMapping(path ="/add", method = RequestMethod.POST)
  public String addBeverageSubmit(
          @RequestParam(required = false) String jenis_minuman,
          @RequestParam(required = false) String jenis_penyajian,
          @RequestParam(required = false) List<String> extras
  ){
        System.out.println(jenis_minuman);
        System.out.println(jenis_penyajian);
        System.out.println(extras);
    Beverage beverage = beverageService.createBeverage(jenis_minuman, jenis_penyajian, extras);
        String className = beverage.getClass().getSimpleName();
        System.out.println(className);
    BeverageModel beverageModel = beverageService.addBeverage(beverage);
    transactionService.addTransaction(beverageModel.getId());

    return "redirect:/transaction";
  }
}
