package id.ac.ui.cs.advprog.a10.kafein.transaction.core;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;

public class ReadyState implements TransactionStateOperations {
  @Override
  public TransactionState checkout(TransactionModel transaction) {
    System.out.println("Order sudah diterima sebelumnya");
    return null;
  }

  @Override
  public TransactionState setReady(TransactionModel transaction) {
    System.out.println("Order sedang dalam proses pembuatan");
    return null;
  }

  @Override
  public TransactionState closeOrder(TransactionModel transaction) {
    System.out.println("Transaksi selesai");
    return TransactionState.DONE;
  }
}
