package id.ac.ui.cs.advprog.a10.kafein.coffeedecoration;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Beverage;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Coffee;
import java.util.ArrayList;

public abstract class CoffeeDecor implements Coffee {
  protected Coffee coffee;

  public CoffeeDecor(Coffee coffee) {
    this.coffee = coffee;
  }

  public String getSpecification() {
    return this.coffee.getSpecification();
  }

  public String getName() {
    return this.coffee.getName();
  }

  public int getPrice() {
    return this.coffee.getPrice();
  }

  public ArrayList<Beverage> getStructure() {
    return this.coffee.getStructure();
  }

}
