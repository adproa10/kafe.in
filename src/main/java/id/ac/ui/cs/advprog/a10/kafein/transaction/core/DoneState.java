package id.ac.ui.cs.advprog.a10.kafein.transaction.core;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;

public class DoneState implements TransactionStateOperations {
  @Override
  public TransactionState checkout(TransactionModel transaction) {
    System.out.println("Transaksi sudah selesai");
    return null;
  }

  @Override
  public TransactionState setReady(TransactionModel transaction) {
    System.out.println("Transaksi sudah selesai");
    return null;
  }

  @Override
  public TransactionState closeOrder(TransactionModel transaction) {
    System.out.println("Transaksi sudah selesai");
    return null;
  }
}
