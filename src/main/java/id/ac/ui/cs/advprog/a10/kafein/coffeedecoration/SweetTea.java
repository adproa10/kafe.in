package id.ac.ui.cs.advprog.a10.kafein.coffeedecoration;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Beverage;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Tea;
import java.util.ArrayList;

public class SweetTea extends TeaDecor {

  public SweetTea(Tea tea) {
    super(tea);
    System.out.println("Please Wait...");
    System.out.println("Adding Sugar to The Tea...");

  }

  public String getSpecification() {
    return this.tea.getSpecification() + " With Extra Milk!!";
  }

  public String getName() {
    return "Sweet " + this.tea.getName();
  }

  public int getPrice() {
    return this.tea.getPrice() + 2000;
  }

  public ArrayList<Beverage> getStructure() {
    return this.tea.getStructure();
  }

  @Override
  public void setName(String name) {

  }

  @Override
  public void setBasePrice(int basePrice) {

  }

}
