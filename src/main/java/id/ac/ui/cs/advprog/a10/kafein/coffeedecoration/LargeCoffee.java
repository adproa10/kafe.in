package id.ac.ui.cs.advprog.a10.kafein.coffeedecoration;


import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Beverage;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Coffee;
import java.util.ArrayList;

public class LargeCoffee extends CoffeeDecor {

  public LargeCoffee(Coffee coffee) {
    super(coffee);
    System.out.println("Please Wait...");
    System.out.println("Adding Size to The Coffee...");

  }

  public String getSpecification() {
    return this.coffee.getSpecification() + " With Extra Size!!";
  }

  public String getName() {
    return "Large " + this.coffee.getName();
  }

  public int getPrice() {
    return this.coffee.getPrice() + 5500;
  }

  public ArrayList<Beverage> getStructure() {
    return this.coffee.getStructure();
  }

  @Override
  public void setName(String name) {
  }

  @Override
  public void setBasePrice(int basePrice) {

  }

}
