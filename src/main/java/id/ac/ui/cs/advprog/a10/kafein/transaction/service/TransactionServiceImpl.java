package id.ac.ui.cs.advprog.a10.kafein.transaction.service;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import id.ac.ui.cs.advprog.a10.kafein.transaction.repository.TransactionRepository;
import id.ac.ui.cs.advprog.a10.kafein.users.core.User;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionServiceImpl implements TransactionService {
  User store = new User("Nama", "Email", "pass", "STORE");
  User user = new User("Nama User", "Email User", "pass", "USER");

  @Autowired
  public TransactionRepository transactionRepository;

  @Override
  public List<TransactionModel> findAll() {
    return transactionRepository.findAll();
  }

  @Override
  public Optional<TransactionModel> getTransaction(int id) {
    return transactionRepository.findById(id);
  }

  @Override
  public void deleteTransaction(int id) {
    transactionRepository.deleteById(id);
  }

  @Override
  public void updateTransaction(TransactionModel transactionModel) {
    String state = transactionModel.getTransactionState().name();
    if (state == "CART") {
      transactionModel.checkout();
    } else if (state == "CHECKED_OUT") {
      transactionModel.setReady();
    } else if (state == "READY") {
      transactionModel.closeOrder();
    } else {
      ;
    }
    transactionRepository.save(transactionModel);
  }

  @Override
  public TransactionModel addTransaction(Long beverageId) {
    TransactionModel transactionModel = new TransactionModel(user.getEmail(),
                                        store.getEmail(), beverageId);
    user.addTransaction(transactionModel.getId());
    TransactionModel t = transactionRepository.save(transactionModel);
    return transactionModel;
  }

}
