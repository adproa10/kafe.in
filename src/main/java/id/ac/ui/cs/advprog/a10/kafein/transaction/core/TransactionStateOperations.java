package id.ac.ui.cs.advprog.a10.kafein.transaction.core;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;

public interface TransactionStateOperations {

  public TransactionState checkout(TransactionModel transaction);

  public TransactionState setReady(TransactionModel transaction);

  public TransactionState closeOrder(TransactionModel transaction);

}
