package id.ac.ui.cs.advprog.a10.kafein.transaction.core;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;

public class CartState implements TransactionStateOperations {

  @Override
  public TransactionState checkout(TransactionModel transaction) {
    System.out.println("Order diterima oleh Kafe.in, mohon ditunggu...");
    return TransactionState.CHECKED_OUT;
  }

  @Override
  public TransactionState setReady(TransactionModel transaction) {
    System.out.println("Order belum diterima oleh Kafe.in");
    return null;
  }

  @Override
  public TransactionState closeOrder(TransactionModel transaction) {
    System.out.println("Order belum diterima oleh Kafe.in");
    return null;
  }

}
