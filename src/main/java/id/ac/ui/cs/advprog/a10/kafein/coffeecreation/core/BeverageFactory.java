package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

public interface BeverageFactory {
    public Coffee createCoffee(String name, int basePrice);

    public Tea createTea(String name, int basePrice);
}
