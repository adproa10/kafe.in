package id.ac.ui.cs.advprog.a10.kafein.coffeedecoration;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Beverage;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Coffee;
import java.util.ArrayList;

public class MilkCoffee extends CoffeeDecor {

  public MilkCoffee(Coffee coffee) {
    super(coffee);
    System.out.println("Please Wait...");
    System.out.println("Adding Milk to The Coffee...");

  }

  public String getSpecification() {
    return this.coffee.getSpecification() + " With Fresh Milk!!";
  }

  public String getName() {
    return "Milky " + this.coffee.getName();
  }

  public int getPrice() {
    return this.coffee.getPrice() + 2500;
  }

  public ArrayList<Beverage> getStructure() {
    return this.coffee.getStructure();
  }

  @Override
  public void setName(String name) {

  }

  @Override
  public void setBasePrice(int basePrice) {

  }

}
