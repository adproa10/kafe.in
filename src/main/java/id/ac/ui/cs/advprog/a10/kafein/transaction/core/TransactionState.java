package id.ac.ui.cs.advprog.a10.kafein.transaction.core;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;

public enum TransactionState implements TransactionStateOperations {
  CART(new CartState()),
  CHECKED_OUT(new CheckoutState()),
  READY(new ReadyState()),
  DONE(new DoneState());

  private final TransactionStateOperations operations;

  TransactionState(TransactionStateOperations operations) {
    this.operations = operations;
  }

  @Override
  public TransactionState checkout(TransactionModel transaction) {
    return operations.checkout(transaction);
  }

  @Override
  public TransactionState setReady(TransactionModel transaction) {
    return operations.setReady(transaction);
  }

  @Override
  public TransactionState closeOrder(TransactionModel transaction) {
    return operations.closeOrder(transaction);
  }
}
