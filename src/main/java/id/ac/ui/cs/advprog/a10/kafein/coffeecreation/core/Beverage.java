package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

import java.util.ArrayList;

public interface Beverage {
    public String getName();

    public int getPrice();

    public String getSpecification();

    public ArrayList<Beverage> getStructure();

    public void setName(String name);

    public void setBasePrice(int basePrice);

}