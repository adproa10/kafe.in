package id.ac.ui.cs.advprog.a10.kafein.transaction.repository;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<TransactionModel, Integer> {
}
