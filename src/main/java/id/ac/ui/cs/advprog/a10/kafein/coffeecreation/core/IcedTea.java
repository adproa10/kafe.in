package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

import java.util.ArrayList;

public class IcedTea implements Tea {

    private String name;
    private int basePrice;
    private String spec = "Iced";

    public IcedTea(String name, int basePrice) {
        this.name = name;
        this.basePrice = basePrice;
    }

    @Override
    public String getName() {
        return "Iced Tea";
    }

    @Override
    public int getPrice() {
        return basePrice+6000;
    }

    @Override
    public String getSpecification() {
        return "Iced Tea";
    }

    @Override
    public ArrayList<Beverage> getStructure() {
        return null;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setBasePrice(int basePrice) {
        this.basePrice = basePrice;
    }
}
