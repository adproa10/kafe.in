package id.ac.ui.cs.advprog.a10.kafein.coffeedecoration;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Beverage;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Tea;
import java.util.ArrayList;

public abstract class TeaDecor implements Tea {
  protected Tea tea;

  public TeaDecor(Tea tea) {
    this.tea = tea;
  }

  public String getSpecification() {
    return this.tea.getSpecification();
  }

  public String getName() {
    return this.tea.getName();
  }

  public int getPrice() {
    return this.tea.getPrice();
  }

  public ArrayList<Beverage> getStructure() {
    return this.tea.getStructure();
  }

}
