package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

public class IcedBeverageFactory implements BeverageFactory {
    @Override
    public Coffee createCoffee(String name, int basePrice) {
        return new IcedCoffee(name, basePrice);
    }

    @Override
    public Tea createTea(String name, int basePrice) {
        return new IcedTea(name, basePrice);
    }
}
