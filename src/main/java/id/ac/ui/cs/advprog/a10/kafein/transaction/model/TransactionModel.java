package id.ac.ui.cs.advprog.a10.kafein.transaction.model;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model.BeverageModel;
import id.ac.ui.cs.advprog.a10.kafein.transaction.core.*;
import javax.persistence.*;

@Entity
@Table(name = "transaction")
public class TransactionModel {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @Column(name = "userEmail")
  private String userEmail;

  @Column(name = "storeEmail")
  private String storeEmail;

  @Enumerated(EnumType.STRING)
  private TransactionState transactionState = TransactionState.CART;

  @Column(name = "beverageId")
  private Long beverageId;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "beverageId", referencedColumnName = "id",
              insertable = false, updatable = false)
  private BeverageModel beverageModel;

  public TransactionModel() {
  }

  public TransactionModel(String userEmail, String storeEmail, Long beverageId) {
    this.userEmail = userEmail;
    this.storeEmail = storeEmail;
    this.beverageId = beverageId;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUserEmail() {
    return this.userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }

  public String getStoreEmail() {
    return storeEmail;
  }

  public void setStoreEmail(String storeEmail) {
    this.storeEmail = storeEmail;
  }

  public Long getBeverageId() {
    return beverageId;
  }

  public void setBeverageId(Long beverageId) {
    this.beverageId = beverageId;
  }

  public String getBeverageName() {
    return this.beverageModel.getName();
  }

  public int getBeveragePrice() {
    return this.beverageModel.getPrice();
  }

  public String getBeverageSpecification() {
    return this.beverageModel.getSpecification();
  }

  public TransactionState getTransactionState() {
    return transactionState;
  }

  public void setTransactionState(TransactionState transactionState) {
    this.transactionState = transactionState;
  }

  public void changeState(TransactionState state) {
    this.transactionState = state;
  }

  public void checkout() {
    changeState(transactionState.checkout(this));
  }

  public void setReady() {
    changeState(transactionState.setReady(this));
  }

  public void closeOrder() {
    changeState(transactionState.closeOrder(this));
  }
}
