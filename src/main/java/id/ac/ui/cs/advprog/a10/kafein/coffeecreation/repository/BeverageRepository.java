package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.repository;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model.BeverageModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BeverageRepository  extends JpaRepository<BeverageModel, Long> {
}