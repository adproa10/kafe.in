package id.ac.ui.cs.advprog.a10.kafein.coffeedecoration;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Beverage;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Tea;
import java.util.ArrayList;

public class LargeTea extends TeaDecor {

  public LargeTea(Tea tea) {
    super(tea);
    System.out.println("Please Wait...");
    System.out.println("Adding Size to The Tea...");

  }

  public String getSpecification() {
    return this.tea.getSpecification() + " With Extra Size!!";
  }

  public String getName() {
    return "Large " + this.tea.getName();
  }

  public int getPrice() {
    return this.tea.getPrice() + 5000;
  }

  public ArrayList<Beverage> getStructure() {
    return this.tea.getStructure();
  }

  @Override
  public void setName(String name) {

  }

  @Override
  public void setBasePrice(int basePrice) {

  }

}
