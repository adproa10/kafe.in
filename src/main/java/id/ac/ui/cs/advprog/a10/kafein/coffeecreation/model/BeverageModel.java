package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;

import javax.persistence.*;


@Entity
@Table(name="beverage")
public class BeverageModel {
  public enum Type{
    TEA, COFFEE
  }
  public enum Served{
    HOT, ICED, MILK, LARGE, SWEET
  }

  public BeverageModel() {

  }

  public BeverageModel(String name, String specification, int price, Type type, Served served) {
    this.name = name;
    this.specification = specification;
    this.price = price;
    this.type = type;
    this.served = served;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name="name")
  private String name;

  @Column(name="specification")
  private String specification;

  @Column(name="price")
  private int price;

  @Enumerated(EnumType.ORDINAL)
  private Type type;

  @Enumerated(EnumType.ORDINAL)
  private Served served;

  @OneToOne(mappedBy = "beverageModel")
  private TransactionModel transactionModel;

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public Served getServed() {
    return served;
  }

  public void setServed(Served served) {
    this.served = served;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSpecification() {
    return specification;
  }

  public void setSpecification(String specification) {
    this.specification = specification;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public TransactionModel getTransactionModel() {
    return transactionModel;
  }

  public void setTransactionModel(TransactionModel transactionModel) {
    this.transactionModel = transactionModel;
  }
}
