package id.ac.ui.cs.advprog.a10.kafein.users.core;

public interface TransactionObserver {
  void addTransaction(int transactionId);
  void notify(int transactionId, String message);
}
