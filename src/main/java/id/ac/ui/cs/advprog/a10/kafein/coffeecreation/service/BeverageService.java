package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.service;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Beverage;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model.BeverageModel;

import java.util.List;
import java.util.Optional;

public interface BeverageService {
  public List<BeverageModel> findAll();
  public Optional<BeverageModel> findBeverage(Long id);
  public void erase(Long id); //delete
  public BeverageModel rewrite(BeverageModel beverage); //update
  public BeverageModel register(BeverageModel beverage); //create
  public Beverage createBeverage(String jenis, String penyajian, List<String> extras);
  public BeverageModel addBeverage(Beverage beverage);
}
