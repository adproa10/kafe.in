package id.ac.ui.cs.advprog.a10.kafein.transaction.service;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import java.util.List;
import java.util.Optional;

public interface TransactionService {
  public List<TransactionModel> findAll();

  public Optional<TransactionModel> getTransaction(int id);

  public void deleteTransaction(int id);

  public void updateTransaction(TransactionModel transactionModel);

  public TransactionModel addTransaction(Long beverageId);
}
