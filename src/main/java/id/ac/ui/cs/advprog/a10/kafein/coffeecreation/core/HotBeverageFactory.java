package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

public class HotBeverageFactory implements BeverageFactory {
    @Override
    public Coffee createCoffee(String name, int basePrice) {
        return new HotCoffee(name, basePrice);
    }

    @Override
    public Tea createTea(String name, int basePrice) {
        return new HotTea(name, basePrice);
    }
}