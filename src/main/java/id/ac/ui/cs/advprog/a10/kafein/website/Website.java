package id.ac.ui.cs.advprog.a10.kafein.website;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class Website {
  @RequestMapping(path = "/", method = RequestMethod.GET)
  public String home(Model model) {
    return "website/home";
  }
}
