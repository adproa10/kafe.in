package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotCoffee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HotCoffeeTest {

    private id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotCoffee HotCoffee;
    @BeforeEach
    public void setUp(){
        HotCoffee = new HotCoffee("Hot Coffee", 20000);
    }

    @Test
    public void testHotCoffeegetName(){
        assertEquals("Hot Coffee", HotCoffee.getName());
    }

    @Test
    public void testHotCoffeegetPrice(){
        assertEquals(27000, HotCoffee.getPrice());
    }

    @Test
    public void testHotCoffeegetSpecification(){
        assertEquals("Hot Coffee", HotCoffee.getSpecification());
    }

    @Test
    public void testGetStructure(){
        assertEquals(null, HotCoffee.getStructure());
    }

    @Test
    public void testSetName(){
        HotCoffee v1 = new HotCoffee("Hot Coffee", 27000);
        v1.setName("Hot Coffee");
        assertEquals("Hot Coffee", v1.getName());
    }

    @Test
    public void testSetBasePrice(){
        HotCoffee v1 = new HotCoffee("Hot Coffee", 20000);
        v1.setBasePrice(20000);
        assertEquals(27000, v1.getPrice());
    }

}

