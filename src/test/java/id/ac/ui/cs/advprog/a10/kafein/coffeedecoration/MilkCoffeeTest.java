package id.ac.ui.cs.advprog.a10.kafein.coffeedecoration;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotCoffee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MilkCoffeeTest {
  private MilkCoffee coffee;

  @BeforeEach
  public void setUp() {
    coffee = new MilkCoffee(new HotCoffee("Hot Coffee", 20000));
  }

  @Test
  public void testMethodGetName() {
    //TODO: Complete me
    assertEquals("Milky Hot Coffee", coffee.getName());
  }

  @Test
  public void testMethodGetSpec() {
    //TODO: Complete me
    assertEquals("Hot Coffee With Fresh Milk!!", coffee.getSpecification());
  }

  @Test
  public void testMethodGetPrice() {
    //TODO: Complete me
    assertEquals(29500, coffee.getPrice());
  }

  @Test
  public void testMethodGetStructure() {
    //TODO: Complete me
    assertEquals(null, coffee.getStructure());
  }

  @Test
  public void testMethodSetBasePrice() {
    //TODO: Complete me
    assertEquals(null, coffee.getStructure());
  }
}
