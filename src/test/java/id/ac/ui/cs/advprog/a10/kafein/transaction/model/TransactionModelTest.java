package id.ac.ui.cs.advprog.a10.kafein.transaction.model;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model.BeverageModel;
import id.ac.ui.cs.advprog.a10.kafein.transaction.core.TransactionState;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionModelTest {
  private BeverageModel beverageModel = new BeverageModel("beverage", "specification", 20000, BeverageModel.Type.valueOf("COFFEE"), BeverageModel.Served.valueOf("HOT"));
  private TransactionModel transactionModel = new TransactionModel("user@email.com", "store@email.com", beverageModel.getId());

  @Test
  public void testSetId() {
    transactionModel.setId(100);
    assertEquals(100, transactionModel.getId());
  }

  @Test
  public void testGetUserEmail() {
    assertEquals("user@email.com", transactionModel.getUserEmail());
  }

  @Test
  public void testSetUserEmail() {
    transactionModel.setUserEmail("user@email2.com");
    assertEquals("user@email2.com", transactionModel.getUserEmail());
  }

  @Test
  public void testGetStoreEmail() {
    assertEquals("store@email.com", transactionModel.getStoreEmail());
  }

  @Test
  public void testSetStoreEmail() {
    transactionModel.setStoreEmail("store@email2.com");
    assertEquals("store@email2.com", transactionModel.getStoreEmail());
  }

  @Test
  public void testGetBeverageId() {
    assertEquals(beverageModel.getId(), transactionModel.getBeverageId());
  }

  @Test
  public void testSetBeverageId() {
    transactionModel.setBeverageId(100L);
    assertEquals(100L, transactionModel.getBeverageId());
  }

  @Test
  public void testGetTransactionState() {
    assertEquals("CART", transactionModel.getTransactionState().name());
  }

  @Test
  public void testSetTransactionState() {
    transactionModel.setTransactionState(TransactionState.CHECKED_OUT);
    assertEquals("CHECKED_OUT", transactionModel.getTransactionState().name());
  }

  @Test
  public void testCheckout() {
    transactionModel.checkout();
    assertEquals("CHECKED_OUT", transactionModel.getTransactionState().name());
  }

  @Test
  public void testSetReady() {
    transactionModel.checkout();
    transactionModel.setReady();
    assertEquals("READY", transactionModel.getTransactionState().name());
  }

  @Test
  public void testCloseOrder() {
    transactionModel.checkout();
    transactionModel.setReady();
    transactionModel.closeOrder();
    assertEquals("DONE", transactionModel.getTransactionState().name());
  }
}
