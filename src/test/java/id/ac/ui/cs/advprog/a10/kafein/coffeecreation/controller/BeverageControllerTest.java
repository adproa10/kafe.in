package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.controller;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.service.BeverageService;
import id.ac.ui.cs.advprog.a10.kafein.transaction.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
public class BeverageControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private BeverageService beverageService;

  @MockBean
  private TransactionService transactionService;

  @Test
  public void testAddBeverageGet() throws Exception{
    mockMvc.perform(get("/beverage/add"))
            .andExpect(status().isOk())
            .andExpect(view().name("website/add-beverage"));
  }
}
