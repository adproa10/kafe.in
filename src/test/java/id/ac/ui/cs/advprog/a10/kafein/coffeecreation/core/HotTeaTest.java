package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotTea;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HotTeaTest {

    private id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotTea HotTea;
    @BeforeEach
    public void setUp(){
        HotTea = new HotTea("Hot Tea", 19000);
    }

    @Test
    public void testHotCoffeegetName(){
        assertEquals("Hot Tea", HotTea.getName());
    }

    @Test
    public void testHotCoffeegetPrice(){
        assertEquals(23000, HotTea.getPrice());
    }

    @Test
    public void testHotTeaegetSpecification(){
        assertEquals("Hot Tea", HotTea.getSpecification());
    }

    @Test
    public void testGetStructure(){
        assertEquals(null, HotTea.getStructure());
    }

    @Test
    public void testSetName(){
        HotTea v1 = new HotTea("Hot Tea", 19000);
        v1.setName("Hot Tea");
        assertEquals("Hot Tea", v1.getName());
    }

    @Test
    public void testSetBasePrice(){
        HotTea v1 = new HotTea("Hot Tea", 15000);
        v1.setBasePrice(15000);
        assertEquals(19000, v1.getPrice());
    }

}

