package id.ac.ui.cs.advprog.a10.kafein.users;

import id.ac.ui.cs.advprog.a10.kafein.users.core.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest {

  private User customer;
  private User store;

  @BeforeEach
  public void setUp() {
    customer = new User("Rafi", "rafi@kafein.com", "secret0011");
    store = new User("TJB001", "tjb001@kafein.com", "secret0011", "STORE");
  }

  @Test
  public void testGetName() {
    assertEquals("Rafi", customer.getName());
  }

  @Test
  public void testGetEmail() {
    assertEquals("rafi@kafein.com", customer.getEmail());
  }

  @Test
  public void testGetPassword() {
    assertEquals("secret0011", customer.getPassword());
  }

  @Test
  public void testGetCustomerRole() {
    assertEquals("CUSTOMER", customer.getRole());
  }

  @Test
  public void testGetStoreRole() {
    assertEquals("STORE", store.getRole());
  }

  @Test
  public void testSetNameNotNull() {
    customer.setName("MD");
    assertEquals("MD", customer.getName());
  }

  @Test
  public void testSetNameNullNoEffect() {
    customer.setName(null);
    assertNotNull(customer.getName());
  }

  @Test
  public void testSetNameEmptyNoEffect() {
    customer.setName("");
    assertNotEquals("", customer.getName());
  }

  @Test
  public void testSetPasswordValid() {
    customer.setPassword("sksksk");
    assertEquals("sksksk", customer.getPassword());
  }

  @Test
  public void testSetPasswordNullNoEffect() {
    customer.setPassword(null);
    assertNotNull(customer.getPassword());
  }

  @Test
  public void testSetPasswordEmptyNoEffect() {
    customer.setPassword("");
    assertNotEquals("", customer.getPassword());
  }

  @Test
  public void testValidatePasswordMatch() {
    assertTrue(customer.validatePassword("secret0011"));
  }

  @Test
  public void testValidatePasswordNotMatch() {
    assertFalse(customer.validatePassword("notsecret"));
  }

  @Test
  public void testInitialTransactionList() {
    assertEquals(0, customer.getTransactionList().size());
  }

  @Test
  public void testAddNewTransaction() {
    customer.addTransaction(1);
    assertTrue(customer.getTransactionList().contains(1));
  }

  @Test
  public void testNotifyTransaction() {
    customer.notify(1, "Ready for Pickup");
  }
}
