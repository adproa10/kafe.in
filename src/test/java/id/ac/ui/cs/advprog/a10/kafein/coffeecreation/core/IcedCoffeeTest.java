package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.IcedCoffee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IcedCoffeeTest {

    private id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.IcedCoffee IcedCoffee;
    @BeforeEach
    public void setUp(){
        IcedCoffee = new IcedCoffee("Iced Coffee", 22000);
    }

    @Test
    public void testHotCoffeegetName(){
        assertEquals("Iced Coffee", IcedCoffee.getName());
    }

    @Test
    public void testHotCoffeegetPrice(){
        assertEquals(31000, IcedCoffee.getPrice());
    }

    @Test
    public void testIcedCoffeegetSpecification(){
        assertEquals("Iced Coffee", IcedCoffee.getSpecification());
    }

    @Test
    public void testGetStructure(){
        assertEquals(null, IcedCoffee.getStructure());
    }

    @Test
    public void testSetName(){
        IcedCoffee v1 = new IcedCoffee("Iced Coffee", 31000);
        v1.setName("Iced Coffee");
        assertEquals("Iced Coffee", v1.getName());
    }

    @Test
    public void testSetBasePrice(){
        IcedCoffee v1 = new IcedCoffee("Iced Coffee", 22000);
        v1.setBasePrice(22000);
        assertEquals(31000, v1.getPrice());
    }
}

