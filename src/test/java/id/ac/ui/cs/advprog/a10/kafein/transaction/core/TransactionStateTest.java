package id.ac.ui.cs.advprog.a10.kafein.transaction.core;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import id.ac.ui.cs.advprog.a10.kafein.users.core.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionStateTest {
  private TransactionState transactionState;
  private TransactionModel transactionModel;
  private final User user = new User("Andi", "bla@gmail.com", "hey", "Buyer");
  private final User store = new User("Cabang Tebet", "cabang@kafein.com", "cabs", "Seller");

  @BeforeEach
  public void setUp() {
    transactionModel = new TransactionModel(user.getEmail(), store.getEmail(), 1L);
  }

  @Test
  public void testCheckout() {
    assertEquals("CHECKED_OUT", transactionState.CART.checkout(transactionModel).name());
  }

  @Test
  public void testSetReady() {
    assertEquals("READY", transactionState.CHECKED_OUT.setReady(transactionModel).name());
  }

  @Test
  public void testCloseOrder() {
    assertEquals("DONE", transactionState.READY.closeOrder(transactionModel).name());
  }

}
