package id.ac.ui.cs.advprog.a10.kafein.transaction.core;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import id.ac.ui.cs.advprog.a10.kafein.users.core.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReadyStateTest {
  private ReadyState readyState;
  private TransactionModel transactionModel;
  private final User user = new User("Andi", "bla@gmail.com", "hey", "Buyer");
  private final User store = new User("Cabang Tebet", "cabang@kafein.com", "cabs", "Seller");
  final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

  @BeforeEach
  public void setUp() {
    readyState = new ReadyState();
    transactionModel = new TransactionModel(user.getEmail(), store.getEmail(), 1L);
    System.setOut(new PrintStream(outContent));
  }

  @Test
  public void testCheckout() {
    readyState.checkout(transactionModel);
    assertEquals("Order sudah diterima sebelumnya\n", outContent.toString());
  }

  @Test
  public void testSetReady() {
    readyState.setReady(transactionModel);
    assertEquals("Order sedang dalam proses pembuatan\n", outContent.toString());
  }

  @Test
  public void testCloseOrder() {
    readyState.closeOrder(transactionModel);
    assertEquals("Transaksi selesai\n", outContent.toString());
  }

}
