package id.ac.ui.cs.advprog.a10.kafein.transaction.controller;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model.BeverageModel;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.service.BeverageService;
import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import id.ac.ui.cs.advprog.a10.kafein.transaction.service.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest
public class TransactionControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private BeverageService beverageService;

  @MockBean
  private TransactionService transactionService;

  private TransactionModel transactionModel;

  @BeforeEach
  public void setUp() {
    transactionModel = transactionService.addTransaction(100L);
  }

  @Test
  public void testTransactionPage() throws Exception{
    mockMvc.perform(get("/transaction"))
            .andExpect(status().isOk())
            .andExpect(view().name("website/transaction"));
  }
}
