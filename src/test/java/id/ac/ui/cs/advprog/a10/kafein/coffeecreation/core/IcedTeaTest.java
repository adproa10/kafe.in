package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.IcedTea;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IcedTeaTest {

    private id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.IcedTea IcedTea;
    @BeforeEach
    public void setUp(){
        IcedTea = new IcedTea("Iced Tea", 17000);
    }

    @Test
    public void testHotCoffeegetName(){
        assertEquals("Iced Tea", IcedTea.getName());
    }

    @Test
    public void testHotCoffeegetPrice(){
        assertEquals(23000, IcedTea.getPrice());
    }

    @Test
    public void testIcedTeagetSpecification(){
        assertEquals("Iced Tea", IcedTea.getSpecification());
    }

    @Test
    public void testGetStructure(){
        assertEquals(null, IcedTea.getStructure());
    }

    @Test
    public void testSetName(){
        IcedTea v1 = new IcedTea("Iced Tea", 23000);
        v1.setName("Iced Tea");
        assertEquals("Iced Tea", v1.getName());
    }

    @Test
    public void testSetBasePrice(){
        IcedTea v1 = new IcedTea("Iced Tea", 17000);
        v1.setBasePrice(17000);
        assertEquals(23000, v1.getPrice());
    }

}

