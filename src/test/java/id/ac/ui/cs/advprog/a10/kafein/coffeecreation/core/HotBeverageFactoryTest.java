package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HotBeverageFactoryTest {
  private HotBeverageFactory hotBeverageFactory = new HotBeverageFactory();
  @Test
  public void CreateCoffeeTest(){
    Coffee a = hotBeverageFactory.createCoffee("Hot Coffee", 20000);
    HotCoffee b = new HotCoffee("Hot Coffee", 20000);
    assertEquals(a.getName(), b.getName());
    assertEquals(a.getPrice(), b.getPrice());
  }

  @Test
  public void CreateTeaTest(){
    Tea a = hotBeverageFactory.createTea("Hot Tea", 15000);
    HotTea b = new HotTea("Hot Tea", 15000);
    assertEquals(a.getName(), b.getName());
    assertEquals(a.getPrice(), b.getPrice());
  }
}
