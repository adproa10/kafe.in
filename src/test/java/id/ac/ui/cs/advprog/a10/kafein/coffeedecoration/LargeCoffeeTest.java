package id.ac.ui.cs.advprog.a10.kafein.coffeedecoration;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotCoffee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LargeCoffeeTest {
  private LargeCoffee largeCoffee;

  @BeforeEach
  public void setUp() {
    largeCoffee = new LargeCoffee(new HotCoffee("Hot Coffee", 20000));
  }

  @Test
  public void testMethodGetName() {
    //TODO: Complete me
    assertEquals("Large Hot Coffee", largeCoffee.getName());
  }

  @Test
  public void testMethodGetSpec() {
    //TODO: Complete me
    assertEquals("Hot Coffee With Extra Size!!", largeCoffee.getSpecification());
  }

  @Test
  public void testMethodGetPrice() {
    //TODO: Complete me
    assertEquals(32500, largeCoffee.getPrice());
  }

  @Test
  public void testMethodGetStructure() {
    //TODO: Complete me
    assertEquals(null, largeCoffee.getStructure());
  }

//    @Test
//    public void testMethodSetBasePrice(){
//        //TODO: Complete me
//        assertEquals(null, largeCoffee.getStructure());
//    }


//    @Test
//    public void testMethodSetName(){
//        //TODO: Complete me
//        assertEquals(null, largeCoffee.getStructure());
//    }


}
