package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class IcedBeverageFactoryTest {
  private IcedBeverageFactory icedBeverageFactory = new IcedBeverageFactory();
  @Test
  public void CreateCoffeeTest(){
    Coffee a = icedBeverageFactory.createCoffee("Iced Coffee", 22000);
    IcedCoffee b = new IcedCoffee("Iced Coffee", 22000);
    assertEquals(a.getName(), b.getName());
    assertEquals(a.getPrice(), b.getPrice());
  }

  @Test
  public void CreateTeaTest(){
    Tea a = icedBeverageFactory.createTea("Iced Tea", 17000);
    IcedTea b = new IcedTea("Iced Tea", 17000);
    assertEquals(a.getName(), b.getName());
    assertEquals(a.getPrice(), b.getPrice());
  }
}
