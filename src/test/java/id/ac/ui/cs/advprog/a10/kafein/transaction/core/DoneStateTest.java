package id.ac.ui.cs.advprog.a10.kafein.transaction.core;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import id.ac.ui.cs.advprog.a10.kafein.users.core.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DoneStateTest {
  private DoneState doneState;
  private TransactionModel transactionModel;
  private final User user = new User("Andi", "bla@gmail.com", "hey", "Buyer");
  private final User store = new User("Cabang Tebet", "cabang@kafein.com", "cabs", "Seller");
  final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

  @BeforeEach
  public void setUp() {
    doneState = new DoneState();
    transactionModel = new TransactionModel(user.getEmail(), store.getEmail(), 1L);
    System.setOut(new PrintStream(outContent));
  }

  @Test
  public void testCheckout() {
    doneState.checkout(transactionModel);
    assertEquals("Transaksi sudah selesai\n", outContent.toString());
  }

  @Test
  public void testSetReady() {
    doneState.setReady(transactionModel);
    assertEquals("Transaksi sudah selesai\n", outContent.toString());
  }

  @Test
  public void testCloseOrder() {
    doneState.closeOrder(transactionModel);
    assertEquals("Transaksi sudah selesai\n", outContent.toString());
  }

}
