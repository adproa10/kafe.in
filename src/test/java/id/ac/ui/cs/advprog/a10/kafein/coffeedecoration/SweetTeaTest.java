package id.ac.ui.cs.advprog.a10.kafein.coffeedecoration;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotTea;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SweetTeaTest {
  private SweetTea tea;

  @BeforeEach
  public void setUp() {
    tea = new SweetTea(new HotTea("Hot Tea", 15000));
  }

  @Test
  public void testMethodGetName() {
    //TODO: Complete me
    assertEquals("Sweet Hot Tea", tea.getName());
  }

  @Test
  public void testMethodGetSpec() {
    //TODO: Complete me
    assertEquals("Hot Tea With Extra Milk!!", tea.getSpecification());
  }

  @Test
  public void testMethodGetPrice() {
    //TODO: Complete me
    assertEquals(21000, tea.getPrice());
  }

  @Test
  public void testMethodGetStructure() {
    //TODO: Complete me
    assertEquals(null, tea.getStructure());
  }

  @Test
  public void testMethodSetBasePrice() {
    //TODO: Complete me
    assertEquals(null, tea.getStructure());
  }
}
