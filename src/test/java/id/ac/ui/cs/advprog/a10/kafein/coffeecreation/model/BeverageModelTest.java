package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model;

import javax.persistence.*;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotBeverageFactory;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotCoffee;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.IcedCoffee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeverageModelTest {
  private BeverageModel a = new BeverageModel("Hot Tea", "Hot Tea", 15000, BeverageModel.Type.TEA, BeverageModel.Served.HOT);

  @Test
  public void TestGetType(){
    assertEquals(BeverageModel.Type.TEA, a.getType());
  }

  @Test
  public void TestSetType(){
    a.setType(BeverageModel.Type.TEA);
    assertEquals(BeverageModel.Type.TEA, a.getType());
  }

  @Test
  public void testSetName(){
    a.setName("Hot Tea");
    assertEquals("Hot Tea", a.getName());
  }

  @Test
  public void testGetName(){
     assertEquals("Hot Tea", a.getName());
  }

  @Test
  public void TestGetServed(){
    assertEquals(BeverageModel.Served.HOT, a.getServed());
  }

  @Test
  public void TestSetServed(){
    a.setServed(BeverageModel.Served.HOT);
    assertEquals(BeverageModel.Served.HOT, a.getServed());
  }

  @Test
  public void testSetSpecification(){
    a.setSpecification("Hot Tea");
    assertEquals("Hot Tea", a.getSpecification());
  }

  @Test
  public void testGetSpecification(){
    assertEquals("Hot Tea", a.getSpecification());
  }

  @Test
  public void testSetPrice(){
    a.setPrice(15000);
    assertEquals(15000, a.getPrice());
  }

  @Test
  public void testGetPrice(){
    assertEquals(15000, a.getPrice());
  }

  @Test
  public void testSetId(){
    a.setId(1);
    assertEquals(1, a.getId());
  }
}
