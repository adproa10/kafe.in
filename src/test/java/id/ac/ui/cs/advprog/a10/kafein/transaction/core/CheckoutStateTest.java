package id.ac.ui.cs.advprog.a10.kafein.transaction.core;

import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import id.ac.ui.cs.advprog.a10.kafein.users.core.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckoutStateTest {
  private CheckoutState checkoutState;
  private TransactionModel transactionModel;
  private final User user = new User("Andi", "bla@gmail.com", "hey", "Buyer");
  private final User store = new User("Cabang Tebet", "cabang@kafein.com", "cabs", "Seller");
  final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

  @BeforeEach
  public void setUp() {
    checkoutState = new CheckoutState();
    transactionModel = new TransactionModel(user.getEmail(), store.getEmail(), 1L);
    System.setOut(new PrintStream(outContent));
  }

  @Test
  public void testCheckout() {
    checkoutState.checkout(transactionModel);
    assertEquals("Order sedang dalam proses pembuatan\n", outContent.toString());
  }

  @Test
  public void testSetReady() {
    checkoutState.setReady(transactionModel);
    assertEquals("Order siap diambil\n", outContent.toString());
  }

  @Test
  public void testCloseOrder() {
    checkoutState.closeOrder(transactionModel);
    assertEquals("Order belum selesai dibuat\n", outContent.toString());
  }

}
