package id.ac.ui.cs.advprog.a10.kafein.transaction.service;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model.BeverageModel;
import id.ac.ui.cs.advprog.a10.kafein.transaction.model.TransactionModel;
import id.ac.ui.cs.advprog.a10.kafein.transaction.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceImplTest {
  @Mock
  private TransactionRepository transactionRepository;

  private BeverageModel beverageModel;
  private TransactionModel transactionModel;

  @InjectMocks
  private TransactionServiceImpl transactionService;

  @BeforeEach
  public void setUp() {
    beverageModel = new BeverageModel("beverage", "specification", 20000, BeverageModel.Type.valueOf("COFFEE"), BeverageModel.Served.valueOf("HOT"));
    transactionModel = transactionService.addTransaction(beverageModel.getId());
  }

  @Test
  public void testFindAll() {
    List<TransactionModel> transactionModelList = transactionService.findAll();
    lenient().when(transactionService.findAll()).thenReturn(transactionModelList);
  }

  @Test
  public void testAddTransaction() {
    lenient().when(transactionService.addTransaction(beverageModel.getId())).thenReturn(transactionModel);
  }

  @Test
  public void testGetTransaction() {
    lenient().when(transactionService.getTransaction(transactionModel.getId())).thenReturn(Optional.of(transactionModel));
  }

  @Test
  public void testUpdateTransaction() {
    transactionService.updateTransaction(transactionModel);
    assertEquals("CHECKED_OUT", transactionModel.getTransactionState().name());
    transactionService.updateTransaction(transactionModel);
    assertEquals("READY", transactionModel.getTransactionState().name());
    transactionService.updateTransaction(transactionModel);
    assertEquals("DONE", transactionModel.getTransactionState().name());
  }

  @Test
  public void testDeleteTransaction() {
    lenient().when(transactionService.addTransaction(beverageModel.getId())).thenReturn(transactionModel);
    transactionService.deleteTransaction(transactionModel.getId());
    assertEquals(0, transactionService.findAll().size());
  }
}
