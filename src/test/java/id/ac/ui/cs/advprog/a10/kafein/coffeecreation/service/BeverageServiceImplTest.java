package id.ac.ui.cs.advprog.a10.kafein.coffeecreation.service;

import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.service.BeverageServiceImpl;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.Beverage;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.core.HotCoffee;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.model.BeverageModel;
import id.ac.ui.cs.advprog.a10.kafein.coffeecreation.repository.BeverageRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class BeverageServiceImplTest {
  @Mock
  private BeverageRepository beverageRepository;

  private BeverageModel beverageModel;
  private Beverage beverage;

  @InjectMocks
  private BeverageServiceImpl beverageService;

  @BeforeEach
  public void setUp() {
    beverageModel = new BeverageModel(
            "Coffee",
            "Coffee",
            20000,
            BeverageModel.Type.COFFEE,
            BeverageModel.Served.HOT
    );
    beverageModel.setId(1);
    beverage = new HotCoffee("coffee", 20000);
  }

  @Test
  public void testFindAll() {
    List<BeverageModel> beverageModelList = beverageService.findAll();
    lenient().when(beverageService.findAll()).thenReturn(beverageModelList);
  }

  @Test
  public void testRegisterBeverage() {
    beverageService.register(beverageModel);
    lenient().when(beverageService.register(beverageModel)).thenReturn(beverageModel);
  }

  @Test
  public void testFindBeverage() {
    beverageService.register(beverageModel);
    Optional<BeverageModel> optionalBeverageModelExist = beverageService.findBeverage(beverageModel.getId());
    Optional<BeverageModel> optionalBeverageModelDoesntExist = beverageService.findBeverage((long) 2);
    lenient().when(beverageService.findBeverage(beverageModel.getId())).thenReturn(Optional.of(beverageModel));
  }

  @Test
  public void testErase() {
    beverageService.register(beverageModel);
    beverageService.erase(beverageModel.getId());
    lenient().when(beverageService.findBeverage(beverageModel.getId())).thenReturn(Optional.empty());
  }

  @Test
  public void testRewriteBeverage() {
    beverageService.register(beverageModel);
    assertEquals(beverageModel, beverageService.rewrite(beverageModel));
  }

  @Test
  public void testCreateBeverage() {
    String penyajian = "hot";
    String jenis = "coffee";
    assertEquals(beverageService.createBeverage(jenis, penyajian, new ArrayList<String>()) instanceof Beverage, true);
  }

  @Test
  public void testCreateBeverage2() {
    String penyajian = "iced";
    String jenis = "tea";
    assertEquals(beverageService.createBeverage(jenis, penyajian, new ArrayList<String>()) instanceof Beverage, true);
  }

  @Test
  public void testAddBeverage() {
    beverageModel = beverageService.addBeverage(beverage);
    lenient().when(beverageService.addBeverage(beverage)).thenReturn(beverageModel);
  }
}
